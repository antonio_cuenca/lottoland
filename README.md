This project has been created as a solution for Lottoland Development Task

## Launching

In the project directory.

Install dependencies:

```
npm install
```

Run development server:

```
npm start
```

Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

## Building Solution

In the project directory.

Build solution:

```
npm run build
```

## Testing

In the project directory.

Run test:

```
npm run test
```

Results will shown in terminal.
