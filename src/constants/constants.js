export const HEADER = 'EUROJACKPOT RESULTS & WINNING NUMBERS';

export const SIDEBAR_CONTENT = {
    TOP: {
        FIRST: 'The ',
        SECOND: 'th draw for the EuroJackpot was held on ',
        THIRD: ', as usual at 9pm in Helsinki.'
    },
    BOTTOM:
        'The balls used for the draw are made of a synthetic polymer, softer than ping-pong balls. The results are broadcast after the draw, with the draw-machines independently checked by the VTT Technical Research Center of Finland.\nLottoland published the draw results immediately after the draw on 08.06.2018. You can easily check your tickets here at Lottoland, or purchase your ticket for the next draw.'
};

export const TABLE_HEADERS = {
    TIER: 'Tier',
    MATCH: 'Match',
    WINNERS: 'Winners',
    AMOUNT: 'Amount'
};

export const MATCHES = [
    {
        tier: 'I',
        match: '5 Numbers +\n2 Euronumbers'
    },
    {
        tier: 'II',
        match: '5 Numbers +\n1 Euronumber'
    },
    {
        tier: 'III',
        match: '5 Numbers +\n0 Euronumbers'
    },
    {
        tier: 'IV',
        match: '4 Numbers +\n2 Euronumbers'
    },
    {
        tier: 'V',
        match: '4 Numbers +\n1 Euronumber'
    },
    {
        tier: 'VI',
        match: '4 Numbers +\n0 Euronumber'
    },
    {
        tier: 'VII',
        match: '3 Numbers +\n2 Euronumbers'
    },
    {
        tier: 'VIII',
        match: '2 Numbers +\n2 Euronumbers'
    },
    {
        tier: 'IX',
        match: '3 Numbers +\n1 Euronumber'
    },
    {
        tier: 'X',
        match: '3 Numbers +\n0 Euronumbers'
    },
    {
        tier: 'XI',
        match: '1 Number +\n2 Euronumbers'
    },
    {
        tier: 'XII',
        match: '2 Numbers +\n1 Euronumber'
    }
];
