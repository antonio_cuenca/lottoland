import React from 'react';
import ReactDOM from 'react-dom';
import EuroJackpot from './scripts/containers/EuroJackpot';

ReactDOM.render(<EuroJackpot />, document.getElementById('app'));
