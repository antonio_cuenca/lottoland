import React from 'react';
import PropTypes from 'prop-types';
import '../../styles/components/ResultsTable.css';
import { TABLE_HEADERS } from '../../constants/constants';

class ResultsTable extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            isMobile: window.innerWidth <= 600
        };

        this.handleResize = this.handleResize.bind(this);
    }

    componentWillMount() {
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize() {
        const isMobile = window.innerWidth <= 600;
        this.setState({ isMobile: isMobile });
    }

    createRowMobile(tier, match, winners, prize) {
        return (
            <tr key={tier} className="result_mobile">
                <td className="result_mobile_left">
                    <p className="tier">{TABLE_HEADERS.TIER + ' ' + tier}</p>
                    <p className="match">{match}</p>
                </td>
                <td className="result_mobile_right">
                    <p className="winners">{winners}</p>
                    <p className="tier">{prize}</p>
                </td>
            </tr>
        );
    }

    createRow(tier, match, winners, prize) {
        return (
            <tr key={tier} className="result">
                <td>{tier}</td>
                <td>{match}</td>
                <td>{winners}</td>
                <td>{prize}</td>
            </tr>
        );
    }

    renderTableHeader() {
        return (
            <thead className="results_head">
                <tr>
                    <th>{TABLE_HEADERS.TIER}</th>
                    <th>{TABLE_HEADERS.MATCH}</th>
                    <th>{TABLE_HEADERS.WINNERS}</th>
                    <th>{TABLE_HEADERS.AMOUNT}</th>
                </tr>
            </thead>
        );
    }

    render() {
        return this.state.isMobile ? (
            <table className="results_table_mobile">
                <tbody className="results_body_mobile">
                    {this.props.results.map(result =>
                        this.createRowMobile(result.tier, result.match, result.winners, result.prize)
                    )}
                </tbody>
            </table>
        ) : (
            <table className="results_table">
                {this.renderTableHeader()}
                <tbody className="results_body">
                    {this.props.results.map(result =>
                        this.createRow(result.tier, result.match, result.winners, result.prize)
                    )}
                </tbody>
            </table>
        );
    }
}

export default ResultsTable;

ResultsTable.propTypes = {
    results: PropTypes.array
};
