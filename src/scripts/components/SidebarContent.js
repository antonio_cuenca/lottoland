import React from 'react';
import PropTypes from 'prop-types';
import '../../styles/components/SidebarContent.css';

class SidebarContent extends React.PureComponent {
    render() {
        return (
            <div className="sidebar_content">
                <div className="title">{this.props.title}</div>
                <div className="body">{this.props.body}</div>
            </div>
        );
    }
}

export default SidebarContent;

SidebarContent.propTypes = {
    title: PropTypes.string,
    body: PropTypes.string
};
