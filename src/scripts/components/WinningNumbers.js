import React from 'react';
import PropTypes from 'prop-types';
import '../../styles/components/WinningNumbers.css';

class WinningNumbers extends React.PureComponent {
    createNumber(number, className) {
        return (
            <li key={number} className={className}>
                {number}
            </li>
        );
    }

    render() {
        return (
            <ul className="winning_numbers">
                {this.props.numbers.map(number => this.createNumber(number, 'number'))}
                {this.props.euroNumbers.map(number => this.createNumber(number, 'number euro'))}
            </ul>
        );
    }
}

export default WinningNumbers;

WinningNumbers.propTypes = {
    numbers: PropTypes.array,
    euroNumbers: PropTypes.array
};
