import React, { Component } from 'react';
import axios from 'axios';
import WinningNumbers from '../components/WinningNumbers';
import ResultsTable from '../components/ResultsTable';
import SidebarContent from '../components/SidebarContent';
import { HEADER, MATCHES, SIDEBAR_CONTENT } from '../../constants/constants';
import '../../styles/containers/EuroJackpot.css';

class EuroJackpot extends Component {
    constructor() {
        super();
        this.state = {
            isDataLoaded: false,
            data: {},
            selectedResult: {}
        };
    }

    fetchData(endpoint) {
        axios
            .get(endpoint)
            .then(response => {
                this.setState({ isDataLoaded: true, data: response.data, selectedResult: response.data.last });
            })
            .catch(err => {
                console.log(err);
            });
    }

    prepareResults(results) {
        var resultsArray = Object.keys(results).map(function(key) {
            return results[key];
        });

        return resultsArray
            .filter(result => result.prize > 0)
            .sort((a, b) => b.prize - a.prize)
            .map((element, index) => {
                return {
                    tier: MATCHES[index].tier,
                    match: MATCHES[index].match,
                    winners: element.winners + 'x',
                    prize: element.prize.toLocaleString('en-US', { style: 'currency', currency: 'EUR' })
                };
            });
    }

    componentDidMount() {
        const proxy = 'https://cors-anywhere.herokuapp.com/';
        const url = 'https://www.lottoland.com/api/drawings/euroJackpot';
        this.fetchData(proxy + url);
    }

    prepareTopSideBarBody(result, date) {
        return (
            SIDEBAR_CONTENT.TOP.FIRST +
            result +
            SIDEBAR_CONTENT.TOP.SECOND +
            new Date(date).toLocaleString('de-ES', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric'
            }) +
            SIDEBAR_CONTENT.TOP.THIRD
        );
    }

    render() {
        return (
            <div>
                {this.state.isDataLoaded ? (
                    <div className="content">
                        <div className="header">{HEADER}</div>
                        <div className="body">
                            <div className="numbers">
                                <div className="date">{this.state.selectedResult.date.full}</div>
                                <WinningNumbers
                                    numbers={this.state.selectedResult.numbers}
                                    euroNumbers={this.state.selectedResult.euroNumbers}
                                />
                            </div>
                            <div className="results">
                                <div className="results_list">
                                    <ResultsTable results={this.prepareResults(this.state.selectedResult.odds)} />
                                </div>
                                <div className="sidebar">
                                    <SidebarContent
                                        title={this.state.selectedResult.date.full}
                                        body={this.prepareTopSideBarBody(
                                            this.state.selectedResult.nr,
                                            this.state.selectedResult.drawingDate
                                        )}
                                    />
                                    <SidebarContent
                                        title={this.state.selectedResult.date.full}
                                        body={SIDEBAR_CONTENT.BOTTOM}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>Loading data...</div>
                )}
            </div>
        );
    }
}

export default EuroJackpot;
