import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import { expect } from 'chai';
import EuroJackpot from '../scripts/containers/EuroJackpot';

describe('EuroJackpot componet should', () => {
    let euroJackpot = null;

    beforeEach(() => {
        euroJackpot = ReactTestUtils.renderIntoDocument(<EuroJackpot />);
    });

    afterEach(() => {
        euroJackpot = null;
    });

    it('render', () => {
        expect(euroJackpot).not.to.be.null;
    });

    it('return an array of elements removing the ones with 0 as prize when "prepareResults" is called', () => {
        const mockedResults = {
            rank0: {
                winners: 0,
                specialPrize: 0,
                prize: 0
            },
            rank1: {
                winners: 0,
                specialPrize: 0,
                prize: 9000000000
            },
            rank2: {
                winners: 5,
                specialPrize: 0,
                prize: 467308320
            }
        };

        const results = euroJackpot.prepareResults(mockedResults);
        expect(results.length).to.be.equal(2);
    });
});
