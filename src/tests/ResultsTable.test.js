import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import { expect } from 'chai';
import ResultsTable from '../scripts/components/ResultsTable';

describe('ResultsTable componet should', () => {
    let resultsTable = null;
    let mockedProps = null;

    beforeEach(() => {
        mockedProps = {
            results: [
                {
                    tier: 'I',
                    match: '5 Numbers +\n2 Euronumbers',
                    winners: '0x',
                    prize: '€9,000,000,000.00'
                },
                {
                    tier: 'II',
                    match: '5 Numbers +\n1 Euronumber',
                    winners: '5x',
                    prize: '€467,308,320.00'
                }
            ]
        };
        resultsTable = ReactTestUtils.renderIntoDocument(<ResultsTable results={mockedProps.results} />);
    });

    afterEach(() => {
        resultsTable = null;
        mockedProps = null;
    });

    it('render', () => {
        expect(resultsTable).not.to.be.null;
    });

    it('receive props', () => {
        const receivedProps = {
            results: resultsTable.props.results
        };

        expect(receivedProps).to.be.deep.equal(mockedProps);
    });

    it('render an element of "results_table" class when mobile mode is not activated', () => {
        const results_table = ReactTestUtils.findRenderedDOMComponentWithClass(resultsTable, 'results_table');
        expect(results_table).not.to.be.null;
    });

    it('render an element of "results_head" class when mobile mode is not activated', () => {
        const results_head = ReactTestUtils.findRenderedDOMComponentWithClass(resultsTable, 'results_head');
        expect(results_head).not.to.be.null;
    });

    it('render an element of "results_body" class when mobile mode is not activated', () => {
        const results_body = ReactTestUtils.findRenderedDOMComponentWithClass(resultsTable, 'results_body');
        expect(results_body).not.to.be.null;
    });

    it('render the same number of elements of "result" class as results passed as props', () => {
        const result = ReactTestUtils.scryRenderedDOMComponentsWithClass(resultsTable, 'result');
        expect(result.length).to.be.equal(mockedProps.results.length);
    });

    it('render an element of "results_table_mobile" class when mobile mode is activated', () => {
        resultsTable.setState({ isMobile: true });

        const results_table_mobile = ReactTestUtils.findRenderedDOMComponentWithClass(
            resultsTable,
            'results_table_mobile'
        );
        expect(results_table_mobile).not.to.be.null;
    });

    it('render an element of "results_body_mobile" class when mobile mode is activated', () => {
        resultsTable.setState({ isMobile: true });

        const results_body_mobile = ReactTestUtils.findRenderedDOMComponentWithClass(
            resultsTable,
            'results_body_mobile'
        );
        expect(results_body_mobile).not.to.be.null;
    });

    it('render the same number of elements of "result_mobile" class as results passed as props', () => {
        resultsTable.setState({ isMobile: true });

        const result_mobile = ReactTestUtils.scryRenderedDOMComponentsWithClass(resultsTable, 'result_mobile');
        expect(result_mobile.length).to.be.equal(mockedProps.results.length);
    });
});
