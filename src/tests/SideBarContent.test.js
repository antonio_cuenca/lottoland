import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import { expect } from 'chai';
import SideBarContent from '../scripts/components/SideBarContent';

describe('SideBarContent componet should', () => {
    let sideBarContent = null;
    let mockedProps = null;

    beforeEach(() => {
        mockedProps = {
            title: 'title',
            body: 'body'
        };
        sideBarContent = ReactTestUtils.renderIntoDocument(
            <SideBarContent title={mockedProps.title} body={mockedProps.body} />
        );
    });

    afterEach(() => {
        sideBarContent = null;
        mockedProps = null;
    });

    it('render', () => {
        expect(sideBarContent).not.to.be.null;
    });

    it('receive props', () => {
        const receivedProps = {
            title: sideBarContent.props.title,
            body: sideBarContent.props.body
        };

        expect(receivedProps).to.be.deep.equal(mockedProps);
    });

    it('render an element of "sidebar_content" class', () => {
        const sidebar_content = ReactTestUtils.findRenderedDOMComponentWithClass(sideBarContent, 'sidebar_content');
        expect(sidebar_content).not.to.be.null;
    });

    it('render an element of "title" class', () => {
        const title = ReactTestUtils.findRenderedDOMComponentWithClass(sideBarContent, 'title');
        expect(title).not.to.be.null;
    });

    it('render an element of "body" class', () => {
        const body = ReactTestUtils.findRenderedDOMComponentWithClass(sideBarContent, 'body');
        expect(body).not.to.be.null;
    });
});
