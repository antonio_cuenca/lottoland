import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import { expect } from 'chai';
import WinningNumbers from '../scripts/components/WinningNumbers';

describe('WinningNumbers componet should', () => {
    let winingNumbers = null;
    let mockedProps = null;

    beforeEach(() => {
        mockedProps = {
            numbers: [5, 13, 22, 36, 39],
            euroNumbers: [9, 10]
        };
        winingNumbers = ReactTestUtils.renderIntoDocument(
            <WinningNumbers numbers={mockedProps.numbers} euroNumbers={mockedProps.euroNumbers} />
        );
    });

    afterEach(() => {
        winingNumbers = null;
        mockedProps = null;
    });

    it('render', () => {
        expect(winingNumbers).not.to.be.null;
    });

    it('receive props', () => {
        const receivedProps = {
            numbers: winingNumbers.props.numbers,
            euroNumbers: winingNumbers.props.euroNumbers
        };

        expect(receivedProps).to.be.deep.equal(mockedProps);
    });

    it('render an element of "winning_numbers" class', () => {
        const winning_numbers = ReactTestUtils.findRenderedDOMComponentWithClass(winingNumbers, 'winning_numbers');
        expect(winning_numbers).not.to.be.null;
    });

    it('render the same elements of "number" class as numbers + euroNumbers passed as props', () => {
        const numbers = ReactTestUtils.scryRenderedDOMComponentsWithClass(winingNumbers, 'number');
        expect(numbers.length).to.be.equal(mockedProps.numbers.length + mockedProps.euroNumbers.length);
    });

    it('render the same elements of "euro" class as euroNumbers passed as props', () => {
        const numbers = ReactTestUtils.scryRenderedDOMComponentsWithClass(winingNumbers, 'euro');
        expect(numbers.length).to.be.equal(mockedProps.euroNumbers.length);
    });
});
